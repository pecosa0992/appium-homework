package andy;

import components.popups.AlertComponent;
import data.ExercisePageHeaderData;
import extentions.AppiumExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import pages.ChatPage;


@ExtendWith(AppiumExtension.class)
public class ChatPage_Test {


  @Test
  public void clickOnOkButton(){
    new AlertComponent()
        .popupShouldBeVisible()
        .clickOnOkButton();
    new AlertComponent()
        .popupShouldBeInvisible();
  }


  @Test()
  public void typeText(){
    new ChatPage()
        .typeMessage("Hello")
        .typedTextShouldBeTheSameAs("Hello");
  }


  @Test
  public void clickOnExerciseButton(){
    new ChatPage()
        .clickOnExerciseButton()
        .exercisePageHeaderShouldBeTheSameAs(ExercisePageHeaderData.EXERCISE_PAGE_HEADER);
  }


}
