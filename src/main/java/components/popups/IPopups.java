package components.popups;

public interface IPopups<T> {
  T popupShouldBeVisible();
  T popupShouldBeInvisible();

}
