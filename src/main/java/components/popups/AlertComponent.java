package components.popups;

import static com.codeborne.selenide.Selenide.$;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import pages.ChatPage;


public class AlertComponent extends AbsPopupBase<AlertComponent> {
  private SelenideElement alertElement = $(By.id("android:id/parentPanel"));


  @Override
  public AlertComponent popupShouldBeVisible() {
    alertElement.shouldBe(Condition.visible);
    return this;
  }

  @Override
  public AlertComponent popupShouldBeInvisible() {
    alertElement.shouldNotBe(Condition.visible);
    return this;
  }

  public ChatPage clickOnOkButton() {
    $("[text='OK']").click();
    return new ChatPage();
  }
}
