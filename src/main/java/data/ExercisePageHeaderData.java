package data;

public enum ExercisePageHeaderData {
  EXERCISE_PAGE_HEADER("Learn 5 new words today");

  private String header;
  ExercisePageHeaderData(String header){
    this.header=header;
  }

  public String getHeader() {
    return header;
  }


}
