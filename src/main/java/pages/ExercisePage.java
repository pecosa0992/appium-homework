package pages;

import static com.codeborne.selenide.Selenide.$;

import com.codeborne.selenide.Condition;
import data.ExercisePageHeaderData;


public class ExercisePage extends AbsBasePage<ExercisePage> {

  public ExercisePage exercisePageHeaderShouldBeTheSameAs(ExercisePageHeaderData expectedText){
    $(String.format("[text='%s']",expectedText.getHeader())).shouldBe(Condition.visible);
    return this;
  }

}
