package pages;

import static com.codeborne.selenide.Selenide.$;

import com.codeborne.selenide.Condition;


public class ChatPage extends AbsBasePage<ChatPage> {

  public ChatPage typeMessage(String text){
    $("[content-desc=Type a message...]").sendKeys(text);
    return this;
  }

  public ChatPage typedTextShouldBeTheSameAs(String text){
    $(String.format("[text='%s']",text)).shouldBe(Condition.visible);
    return this;
  }

  public ExercisePage clickOnExerciseButton(){
    $("[text='Exercise']").click();
    return new ExercisePage();
  }
}
