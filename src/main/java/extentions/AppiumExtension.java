package extentions;

import com.codeborne.selenide.Configuration;
import drivers.AppiumSelenideDriver;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import pages.StartPage;

public class AppiumExtension implements BeforeAllCallback {

  @Override
  public void beforeAll(ExtensionContext extensionContext) throws Exception {
    Configuration.browserSize=null;
    Configuration.browser= AppiumSelenideDriver.class.getName();
    new StartPage()
        .open()
        .clickNextButton()
        .clickNextButton()
        .clickSkippButton();
  }
}
