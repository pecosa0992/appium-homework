package pageobject;

import com.codeborne.selenide.Selenide;

public class AbsPageObject<T> {
  public T open(){
    Selenide.open();
    return (T)this;
  }
}
